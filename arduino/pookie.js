
const { Board, Servo } = require("johnny-five");
const board = new Board();
const amqp = require('amqplib/callback_api');
const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));


board.on("ready", () => {
    const servo = new Servo.Continuous(4);   

    amqp.connect('amqp://username:password@url', function (err, conn) {

        conn.createChannel(function (err, ch) {

            var q = 'pookie_door_status';

            ch.assertQueue("pookie_door", { durable: false });
            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", "pookie_door");
            ch.consume("pookie_door", function (msg) {
                console.log(msg.content.toString())
                close = {
                    "status": "open"
                };
                ch.sendToQueue("pookie_door_status", Buffer.from(JSON.stringify(close)));

                console.log(" [x] Received %s", msg.content.toString());
                servo.ccw();
                sleep(5000).then(() => {
                    // This will execute 5 seconds from now
                    servo.cw();
                    open = {
                        "status": "close"
                    };
                    ch.sendToQueue("pookie_door_status", Buffer.from(JSON.stringify(open)));
                });

            }, { noAck: true });

        });

    });

    
});
