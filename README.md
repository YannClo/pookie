# Pookie
Pookie dans le sas est un projet d'iot.

Le but est de permettre de déverouiller une porte à l'aide de son smartphone selon deux conditions :
- Il faut être authentifier sur l'application mobile
- Il faut être géolicalisé dans un sas virtuel prédéfini.

Une fois ces conditions remplies, un bouton "Déverouiller la porte" se révèle.

## Schema d'architecture
![schema](pookie_schema.png)

## Présentation : 
![presentation](assets/presentation.gif)

## Materiels utilisés
- Un arduino léonardo
- Un servo moteur
- Beaucoup de carton
- De la colle 
- Des câbles
- Le framework Flutter ainsi que la librairie Johnny five
- Les systèmes MongoDB et RabbitMQ
