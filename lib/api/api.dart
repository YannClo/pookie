import 'package:pookiev2/provider/authProvider.dart';
import 'package:pookiev2/shared/toast.dart';

import 'DBConnection.dart';

class Api {
  final AuthProvider _authProvider = AuthProvider();

  Future<bool> authentification(String login, String mdp) async {
    var db = await DBConnection().getConnection();
    var auth = await db.collection('pookinnection').findOne({"login": login});
    if (!(auth == null)) {
      if ((auth["login"] == login) && (auth["password"] == mdp)) {
        print(auth["login"] + " / " + auth["password"]);
        _authProvider.setIsConnected(true);
        showToast(message: 'Connexion réussi. En attente de la porte...');
        return true;
      } else {
        _authProvider.setIsConnected(false);
        showToast(
            message: 'Connexion refusé ! Login ou mot de passe incorrect.');
        return false;
      }
    } else {
      _authProvider.setIsConnected(false);
      showToast(message: 'Connexion refusé ! Login ou mot de passe incorrect.');
      return false;
    }
  }
}
