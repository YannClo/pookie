import 'package:mongo_dart/mongo_dart.dart' show Db, DbCollection;
import 'package:flutter_dotenv/flutter_dotenv.dart';

class DBConnection {
  static DBConnection _instance;

  final String _user = env['MONGODB_USER'];
  final String _password = env['MONGODB_PASSWORD'];
  final String _host = env['MONGODB_HOST'];
  final String _port = env['MONGODB_PORT'];
  final String _dbName = env['MONGODB_DB_NAME'];
  Db _db;

  static getInstance() {
    if (_instance == null) {
      _instance = DBConnection();
    }
    return _instance;
  }

  Future<Db> getConnection() async {
    if (_db == null) {
      try {
        _db = Db(_getConnectionString());
        await _db.open();
      } catch (e) {
        print(e);
      }
    }
    return _db;
  }

  _getConnectionString() {
    return "mongodb://$_user:$_password@$_host:$_port/$_dbName";
  }

  closeConnection() {
    _db.close();
  }
}
