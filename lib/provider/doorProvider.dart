import 'dart:developer';

import 'package:dart_amqp/dart_amqp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pookiev2/models/doorStatus.dart';
import 'package:pookiev2/shared/toast.dart';

import '../globals.dart';
import 'authProvider.dart';
import 'mapProvider.dart';

class DoorProvider extends ChangeNotifier {
  ConnectionSettings _settings = new ConnectionSettings(
      host: env['RABBITMQ_HOST'],
      authProvider: new PlainAuthenticator(
          env['RABBITMQ_USER'], env['RABBITMQ_PASSWORD']));
  bool isOpen = false;

  // Singleton
  static final DoorProvider _doorProvider = DoorProvider._internal();

  DoorProvider._internal();

  factory DoorProvider() {
    return _doorProvider;
  }

  void openDoor() {
    var authProvider = new AuthProvider();
    var mapProvider = new MapProvider();
    var client = Client(settings: _settings);
    client
        .channel()
        .then((Channel channel) =>
            channel.queue(env["RABBITMQ_QUEUE"].toString()))
        .then((Queue queue) {
      if (authProvider.isConnected &&
          mapProvider.isUserInSas &&
          !mapProvider.isPositionSend) {
        mapProvider.setIsPositionSend(true);
        queue.publish(currentPosition.toMap());
      }
    }).catchError((e) {
      showToast(
        message: "Difficultés d'accès au serveur",
      );
      log(e, level: 1);
    });
    notifyListeners();
  }

  void doorState(DoorStatus doorStatus) {
    if (doorStatus.status == "open") {
      isOpen = true;
    } else {
      isOpen = false;
    }
    print("notify");
    notifyListeners();
  }
}
