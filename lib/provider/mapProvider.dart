import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapProvider extends ChangeNotifier {
  String itemId;
  BitmapDescriptor _markerIcon;
  BitmapDescriptor _epsiMarkerIcon;
  bool isLocation = false;
  bool isUserInSas = false;
  bool isPositionSend = false;

  // Singleton
  static final MapProvider _mapProvider = MapProvider._internal();

  factory MapProvider() {
    return _mapProvider;
  }

  MapProvider._internal();

  void changeItem(String _itemId) {
    _mapProvider.itemId = _itemId;
    notifyListeners();
  }

  void reset() {
    _mapProvider.itemId = null;
  }

  Set<Marker> createMarker(latitude, longitude) {
    return <Marker>[
      Marker(
        markerId: MarkerId("Marker position"),
        position: LatLng(latitude, longitude),
        icon: _markerIcon,
      ),
      Marker(
        markerId: MarkerId("Marker EPSI"),
        position: LatLng(47.205811, -1.539633),
        icon: _epsiMarkerIcon,
      )
    ].toSet();
  }

  Future<void> createMarkerImageFromAsset(BuildContext context) async {
    if (_markerIcon == null) {
      final ImageConfiguration imageConfiguration =
          createLocalImageConfiguration(context, size: Size(16.0, 16.0));
      BitmapDescriptor.fromAssetImage(
              imageConfiguration, 'assets/bonhomme2.png')
          .then(_updateBitmap);
    }
    if (_epsiMarkerIcon == null) {
      final ImageConfiguration imageConfiguration =
          createLocalImageConfiguration(context, size: Size(16.0, 16.0));
      BitmapDescriptor.fromAssetImage(imageConfiguration, 'assets/HEP.png')
          .then(_updateBitmapEpsi);
    }
  }

  List<LatLng> _createPoints() {
    final List<LatLng> points = <LatLng>[];
    points.add(_createLatLng(47.204955, -1.536264));
    points.add(_createLatLng(47.204955, -1.541859));
    points.add(_createLatLng(47.207, -1.541859));
    points.add(_createLatLng(47.207, -1.536264));
    points.add(_createLatLng(47.204955, -1.536264));
    return points;
  }

  LatLng _createLatLng(double lat, double lng) {
    return LatLng(lat, lng);
  }

  setPolyline(Map<PolylineId, Polyline> polylines) {
    Polyline myPolyline = Polyline(
      polylineId: PolylineId("1"),
      color: Colors.blue,
      points: _createPoints(),
    );
    polylines[PolylineId("1")] = myPolyline;
  }

  void _updateBitmap(BitmapDescriptor bitmap) {
    if (_markerIcon != bitmap) {
      _markerIcon = bitmap;
      notifyListeners();
    }
  }

  void _updateBitmapEpsi(BitmapDescriptor bitmap) {
    if (bitmap != _epsiMarkerIcon) {
      _epsiMarkerIcon = bitmap;
      notifyListeners();
    }
  }

  void setIsLocation(_isLocation) {
    if (_isLocation != isLocation) {
      isLocation = _isLocation;
      notifyListeners();
    }
  }

  void setIsUserInSas(_isUserInSas) {
    if (_isUserInSas != isUserInSas) {
      isUserInSas = _isUserInSas;
      notifyListeners();
    }
  }

  void setIsPositionSend(_isPositionSend) {
    if (_isPositionSend != isPositionSend) {
      isPositionSend = _isPositionSend;
      notifyListeners();
    }
  }
}
