import 'package:flutter/material.dart';

class AuthProvider extends ChangeNotifier {
  bool isConnected = false;

  // Singleton
  static final AuthProvider _authProvider = AuthProvider._internal();

  factory AuthProvider() {
    return _authProvider;
  }

  AuthProvider._internal();

  void setIsConnected(bool _isConnected) {
    isConnected = _isConnected;
    notifyListeners();
  }
}
