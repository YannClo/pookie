import 'package:flutter/material.dart';
import 'package:location/location.dart';
import "package:dart_amqp/dart_amqp.dart";
import 'package:pookiev2/components/actionSAS.dart';
import 'package:pookiev2/components/googleMapContainer.dart';
import 'package:pookiev2/components/header.dart';
import 'package:pookiev2/components/headerLogo.dart';
import 'package:pookiev2/components/login.dart';
import 'package:pookiev2/globals.dart';
import 'package:pookiev2/models/doorStatus.dart';
import 'package:pookiev2/models/position.dart';
import 'package:pookiev2/provider/authProvider.dart';
import 'package:pookiev2/provider/doorProvider.dart';
import 'package:pookiev2/provider/mapProvider.dart';
import 'package:flutter/rendering.dart';
import 'package:pookiev2/screens/background.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  MapProvider mapProvider;
  AuthProvider authProvider;
  DoorProvider doorProvider;
  ConnectionSettings settings = new ConnectionSettings(
      host: env['RABBITMQ_HOST'],
      authProvider: new PlainAuthenticator(
          env['RABBITMQ_USER'], env['RABBITMQ_PASSWORD']));
  Location location = Location();
  @override
  void initState() {
    mapProvider = MapProvider();
    authProvider = AuthProvider();
    doorProvider = DoorProvider();
    super.initState();
    locationListener();
    doorStatusListener();
  }

  @override
  Widget build(BuildContext context) {
    mapProvider.createMarkerImageFromAsset(context);
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Stack(children: <Widget>[
          Background(
              child: SafeArea(
                  child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 24.0),
            children: <Widget>[
              SizedBox(height: 20.0),
              HeaderLogo(),
              Header(),
              SizedBox(height: 40.0),
              ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  child: Card(
                      margin: EdgeInsets.zero,
                      color: Color(0xFFeeffffff),
                      child: Column(children: <Widget>[
                        Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              children: <Widget>[
                                Login(),
                                ActionSAS(),
                              ],
                            )),
                        GoogleMapContainer(),
                      ]))),
            ],
          ))),
        ]));
  }

  void locationListener() {
    location.onLocationChanged.listen((value) {
      currentPosition =
          Position(latitude: value.latitude, longitude: value.longitude);
      mapProvider.setIsLocation(true);
      if (currentPosition.longitude > -1.541859 &&
          currentPosition.longitude < -1.536264 &&
          currentPosition.latitude > 47.204955 &&
          currentPosition.latitude < 47.207) {
        mapProvider.setIsUserInSas(true);
      } else {
        mapProvider.setIsUserInSas(false);
      }
    });
  }

  void doorStatusListener() {
    var client = Client(settings: settings);
    client
        .channel()
        .then((Channel channel) =>
            channel.queue(env["RABBITMQ_QUEUE_DOOR"].toString()))
        .then((Queue queue) => queue.consume())
        .then((Consumer consumer) {
      consumer.listen((message) {
        print(" [x] Received ${message.payloadAsString}");
        var doorStatus = DoorStatus.fromJson(message.payloadAsJson);
        mapProvider.setIsPositionSend(false);
        doorProvider.doorState(doorStatus);
      });
    }).catchError((e) {
      // log(e, level: 1);
    });
  }
}
