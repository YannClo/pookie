import 'package:flutter/material.dart';
import 'package:pookiev2/provider/doorProvider.dart';
import 'package:provider/provider.dart';
import 'package:pookiev2/provider/authProvider.dart';

import '../provider/mapProvider.dart';

class HeaderLogo extends StatefulWidget {
  @override
  _HeaderLogoState createState() => _HeaderLogoState();
}

class _HeaderLogoState extends State<HeaderLogo> {
  AuthProvider authProvider;
  MapProvider mapProvider;
  DoorProvider doorProvider;

  @override
  Widget build(BuildContext context) {
    return Consumer3<AuthProvider, MapProvider, DoorProvider>(
        builder: (context, ap, mp, dp, child) {
      authProvider = ap;
      mapProvider = mp;
      doorProvider = dp;
      return whichLogo();
    });
  }

  Widget whichLogo() {
    if (mapProvider.isUserInSas) {
      if (authProvider.isConnected && doorProvider.isOpen) {
        return Column(children: [
          Icon(
            Icons.location_on,
            color: Colors.green,
            size: 64,
          ),
          SizedBox(height: 20.0)
        ]);
      } else if (authProvider.isConnected &&
          mapProvider.isPositionSend &&
          !doorProvider.isOpen) {
        return Column(children: [
          Icon(
            Icons.location_on,
            color: Colors.blue[700],
            size: 64,
          ),
          SizedBox(height: 20.0),
        ]);
      }
      return Column(children: [
        Icon(
          Icons.location_on,
          color: Colors.blue[700],
          size: 64,
        ),
        SizedBox(height: 20.0),
      ]);
    } else {
      return Column(children: [
        Icon(
          Icons.location_on,
          color: Colors.red,
          size: 64,
        ),
        SizedBox(height: 20.0),
      ]);
    }
  }
}
