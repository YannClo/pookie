import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pookiev2/api/api.dart';
import 'package:pookiev2/globals.dart';
import 'package:pookiev2/provider/authProvider.dart';
import 'package:pookiev2/provider/doorProvider.dart';
import 'package:pookiev2/provider/mapProvider.dart';
import 'package:provider/provider.dart';

class ActionSAS extends StatefulWidget {
  const ActionSAS();

  @override
  _ActionSASState createState() => _ActionSASState();
}

class _ActionSASState extends State<ActionSAS> {
  double widgetOpacity = 0.0;
  MapProvider mapProvider;
  AuthProvider authProvider;
  DoorProvider doorProvider;

  @override
  Widget build(BuildContext context) {
    return Consumer3<MapProvider, AuthProvider, DoorProvider>(
        builder: (context, mp, ap, dp, child) {
      authProvider = ap;
      mapProvider = mp;
      doorProvider = dp;
      return mp.isUserInSas ? actionSAS() : LinearProgressIndicator();
    });
  }

  Widget actionSAS() {
    return mapProvider.isPositionSend ? waitActionSAS() : showActionSAS();
  }

  Widget showActionSAS() {
    return Padding(
      padding: EdgeInsets.only(top: 12, bottom: 12),
      child: ButtonTheme(
          minWidth: 200,
          height: 50,
          child: RaisedButton(
            child: Text(
              "Ouvrire le SAS",
              style: new TextStyle(color: const Color(0xFFFFFFFF)),
            ),
            color: Theme.of(context).buttonColor,
            splashColor: Color(0xFFFFFFFF),
            highlightColor: Color(0xFFFFFFFF),
            onPressed: () async {
              FocusScope.of(context).requestFocus(new FocusNode());
              Api()
                  .authentification(
                      controllerLogin.text, controllerPassword.text)
                  .then((bool isConnected) =>
                      isConnected ? doorProvider.openDoor() : null);
            },
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            elevation: 2,
            highlightElevation: 8,
          )),
    );
  }

  Widget waitActionSAS() {
    return Padding(
      padding: EdgeInsets.only(top: 12, bottom: 12),
      child: ButtonTheme(
          minWidth: 200,
          height: 50,
          child: RaisedButton(
            child: Text(
              "En attente du sas",
              style: new TextStyle(color: Colors.black),
            ),
            color: Colors.grey[300],
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onPressed: () => {},
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            elevation: 0,
            highlightElevation: 0,
          )),
    );
  }
}
