import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pookiev2/globals.dart';
import 'package:provider/provider.dart';
import 'package:pookiev2/provider/mapProvider.dart';

class GoogleMapContainer extends StatefulWidget {
  @override
  _GoogleMapContainerState createState() => _GoogleMapContainerState();
}

class _GoogleMapContainerState extends State<GoogleMapContainer> {
  MapProvider mapProvider;
  Map<PolylineId, Polyline> polylines = <PolylineId, Polyline>{};
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<MapProvider>(
      builder: (context, mp, child) {
        mapProvider = mp;
        mapProvider.setPolyline(polylines);
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            mapProvider.isLocation ? map() : loadingMap()
            //: Text("Coordonnées:" + currentLocation["latitude"].toString() + " " + currentLocation["longitude"].toString(),style: TextStyle(color:Color(0xFFFFFFFF))),
          ],
        );
      },
    );
  }

  Widget map() {
    return Container(
      height: 200,
      width: MediaQuery.of(context).size.width,
      child: GoogleMap(
        mapType: MapType.terrain,
        initialCameraPosition: CameraPosition(
          target: LatLng(currentPosition.latitude, currentPosition.longitude),
          zoom: 15,
        ),
        gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
          Factory<OneSequenceGestureRecognizer>(
            () => EagerGestureRecognizer(),
          ),
        ].toSet(),
        markers: mapProvider.createMarker(
            currentPosition.latitude, currentPosition.longitude),
        polylines: Set<Polyline>.of(polylines.values),
        onMapCreated: (GoogleMapController controller) {},
      ),
    );
  }

  Widget loadingMap() {
    return Container(
        height: 200,
        color: Colors.grey[300],
        child: Center(
            child: Stack(
          alignment: Alignment.center,
          children: [
            CircularProgressIndicator(),
            Icon(
              Icons.location_off,
              color: Colors.blue,
            )
          ],
        )));
  }
}
