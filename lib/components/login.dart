import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pookiev2/provider/mapProvider.dart';
import 'package:provider/provider.dart';

import '../globals.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  MapProvider mapProvider;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<MapProvider>(builder: (context, mp, child) {
      mapProvider = mp;
      return mapProvider.isUserInSas ? textField() : Container();
    });
  }

  Widget textField() {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8),
            child: TextField(
              controller: controllerLogin,
              autocorrect: false,
              textCapitalization: TextCapitalization.sentences,
              style: new TextStyle(color: const Color(0xFF000000)),
              decoration: InputDecoration(
                labelText: "Login",
                labelStyle: new TextStyle(color: const Color(0xFF000000)),
                filled: true,
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 8),
              child: TextField(
                controller: controllerPassword,
                autocorrect: false,
                textCapitalization: TextCapitalization.sentences,
                style: new TextStyle(color: const Color(0xFF000000)),
                decoration: InputDecoration(
                  labelText: "Password",
                  labelStyle: new TextStyle(color: const Color(0xFF000000)),
                  filled: true,
                ),
                obscureText: true,
              ))
        ]);
  }
}
