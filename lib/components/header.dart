import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pookiev2/provider/authProvider.dart';
import 'package:pookiev2/provider/doorProvider.dart';
import 'package:pookiev2/provider/mapProvider.dart';
import 'package:provider/provider.dart';

class Header extends StatefulWidget {
  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  MapProvider mapProvider;
  AuthProvider authProvider;
  DoorProvider doorProvider;

  var _textStyle =
      TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w700);

  @override
  Widget build(BuildContext context) {
    return Consumer3<AuthProvider, MapProvider, DoorProvider>(
        builder: (context, ap, mp, dp, child) {
      authProvider = ap;
      mapProvider = mp;
      doorProvider = dp;
      return whichText();
    });
  }

  Widget whichText() {
    if (mapProvider.isUserInSas) {
      if (authProvider.isConnected && doorProvider.isOpen) {
        return Text('Y\'a la Pookie dans l\'sas !',
            style: _textStyle, textAlign: TextAlign.center);
      } else if (authProvider.isConnected &&
          mapProvider.isPositionSend &&
          !doorProvider.isOpen) {
        return Text(
          'Pookie connecté ! Veuillez patienter, la porte va s\'ouvrir...',
          style: _textStyle,
          textAlign: TextAlign.center,
        );
      }
      return Text(
        'Vous pouvez vous connecter',
        style: _textStyle,
        textAlign: TextAlign.center,
      );
    } else {
      return Text('Vous n\'êtes pas dans le sas !',
          style: _textStyle, textAlign: TextAlign.center);
    }
  }
}
