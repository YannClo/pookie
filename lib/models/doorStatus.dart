class DoorStatus {
  String status;

  DoorStatus({this.status});

  DoorStatus.fromJson(Map<String, dynamic> json) {
    status = json['status'];
  }

  Map<String, dynamic> toMap() => {'status': status};
}
