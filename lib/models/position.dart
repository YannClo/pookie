class Position {
  double latitude;
  double longitude;

  Position({this.latitude, this.longitude});

  Map<String, dynamic> toMap() =>
      {'latitude': latitude, 'longitude': longitude};
}
