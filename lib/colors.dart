import 'package:flutter/material.dart';


const myPrimaryColor = const Color(0xFF1976d2); 
const myAccentColor = const Color(0xFF1976d2); 
const myErrorColor = const Color(0xFFE91E63); 
const myBackgroundColor = const Color (0x00FFFFFF);
const myTextColor = const Color(0xFFFFFFFF);