import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void showToast(
    {@required String message,
    Color backgroundColor = Colors.grey,
    Color textColor = Colors.black}) {
  Fluttertoast.cancel();
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: backgroundColor.withAlpha(150),
      textColor: textColor,
      fontSize: 16.0);
}
