import 'package:flutter/material.dart';
import 'package:pookiev2/provider/authProvider.dart';
import 'package:pookiev2/provider/doorProvider.dart';
import 'package:pookiev2/provider/mapProvider.dart';
import 'package:pookiev2/screens/home.dart';
import 'package:pookiev2/colors.dart';
import 'package:provider/provider.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;

Future main() async {
  await DotEnv.load(fileName: '.env');
  return runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final ThemeData myAppTheme = buildTheme();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ListenableProvider<MapProvider>(create: (_) => MapProvider()),
          ListenableProvider<AuthProvider>(create: (_) => AuthProvider()),
          ListenableProvider<DoorProvider>(create: (_) => DoorProvider()),
        ],
        builder: (context, child) => MaterialApp(
              title: 'Pookie',
              theme: myAppTheme,
              home: HomePage(),
            ));
  }
}

ThemeData buildTheme() {
  final ThemeData base = ThemeData.light();

  return base.copyWith(
    accentColor: myAccentColor,
    primaryColor: myPrimaryColor,
    buttonColor: myPrimaryColor,
    scaffoldBackgroundColor: myBackgroundColor,
    cardColor: myBackgroundColor,
    errorColor: myErrorColor,
    textTheme: _buildAppTextTheme(base.textTheme),
    primaryTextTheme: _buildAppTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildAppTextTheme(base.accentTextTheme),
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(
        borderRadius: const BorderRadius.all(Radius.elliptical(20, 20)),
        gapPadding: 10.0,
      ),
    ),
  );
}

TextTheme _buildAppTextTheme(TextTheme base) {
  return base
      .copyWith(
        headline5: base.headline5.copyWith(fontWeight: FontWeight.w900),
        headline6: base.headline6
            .copyWith(fontSize: 18.0, fontWeight: FontWeight.w500),
        caption:
            base.caption.copyWith(fontSize: 14.0, fontWeight: FontWeight.w500),
      )
      .apply(
        fontFamily: 'Lato',
        displayColor: myAccentColor,
        bodyColor: myAccentColor,
      );
}
